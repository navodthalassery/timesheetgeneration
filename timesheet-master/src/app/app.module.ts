import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatIconModule, MatDialogModule } from '@angular/material';
import { HttpModule } from '@angular/http';
import { TaskCreateComponent } from './task-create/task-create.component';
import { TimesheetCreateComponent } from './timesheet-create/timesheet-create.component';

const appRoute: Routes = [
  {path: '', component: EmployeeListComponent},
  {path: 'timesheet', component: TimesheetComponent},
  {path: 'addtask', component: TaskCreateComponent},
  {path: 'addtimesheet', component: TimesheetCreateComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent,
    TaskCreateComponent,
    TimesheetCreateComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    
    RouterModule.forRoot(
      appRoute,
      { enableTracing: true } // <-- debugging purposes only
    ),
    CommonModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    MatTableModule,
    HttpModule,
    MatIconModule,
    MatDialogModule,
  ],
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
