import { TimesheetService } from './../services/timesheet.service';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    constructor(
        private employeeService: EmployeeService,
        private timeSh: TimesheetService,
        private router: Router) { }

    ngOnInit() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });
    }

    employeeClick(row) {
        console.log('row ', row);
        this.timeSh.setUserId(row);
        this.router.navigateByUrl('timesheet');
    }
}