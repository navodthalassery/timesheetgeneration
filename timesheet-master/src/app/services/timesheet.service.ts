import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {
  private baseapi = environment.apiUrl;
  userId: Number = 0;
  private subject = new BehaviorSubject('');
  options: any;

  constructor(private http: HttpClient,
    private httprx: Http) {
    const header1 = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: header1 });
  }

  gettimesheet(id) {
      return this.http.get(this.baseapi + "Timesheet/GetTimesheet?userId="+id);
  }

  setTask(data) {
    console.log('setTask ', data);
    this.httprx.post(this.baseapi + "Timesheet/SetTask", data, this.options)
    .map(res => res.json())
    .subscribe(res => {
      console.log(res);
    });
  }

  setTimesheet(data) {
    console.log('setTask ', data);
    this.httprx.post(this.baseapi + "Timesheet/SetTimesheet", data, this.options)
    .map(res => res.json())
    .subscribe(res => {
      console.log(res);
    });
  }

  setUserId(userDet: any): void {
    this.subject.next(userDet);
  }

  getUserId(): Observable<any> {
    return this.subject.asObservable();
  }
}
