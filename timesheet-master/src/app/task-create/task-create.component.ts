import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TimesheetService } from '../services/timesheet.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss']
})
export class TaskCreateComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder,
    private router: Router,
    private tsTime: TimesheetService) { }

  ngOnInit() {

    this.form = this.fb.group({
      Name: ['', Validators.required],
      Description: ['', Validators.required],
    });
  }

  save() {
    const datas = JSON.stringify(this.form.value);
    console.log('this.form ', datas);
    this.tsTime.setTask(datas);
    this.router.navigateByUrl('timesheet');
  }

}
