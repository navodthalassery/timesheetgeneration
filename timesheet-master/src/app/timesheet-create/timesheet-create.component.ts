import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TimesheetService } from '../services/timesheet.service';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-timesheet-create',
  templateUrl: './timesheet-create.component.html',
  styleUrls: ['./timesheet-create.component.scss']
})
export class TimesheetCreateComponent implements OnInit {
  employees: any;
  employee: any;
  form: FormGroup;
  tasks: any;
  task: any;

  constructor(private router: Router,
    private tsTime: TimesheetService,
    private fb: FormBuilder,
    private empservice: EmployeeService) { }

  ngOnInit() {
    this.empservice.getallemployees().subscribe(data => {
      this.employees = data;
    });
    this.empservice.getalltasks().subscribe(data => {
      this.tasks = data;
    });

    this.form = this.fb.group({
      TaskId: ['', Validators.required],
      EmployeeId: ['', Validators.required],
      Sunday: ['', Validators.required],
      Monday: ['', Validators.required],
      Tuesday: ['', Validators.required],
      Wednesday: ['', Validators.required],
      Thursday: ['', Validators.required],
      Friday: ['', Validators.required],
      Saturday: ['', Validators.required],
    });
  }

  save() {
    const datas = JSON.stringify(this.form.value);
    console.log('this.form ', datas);
    this.tsTime.setTimesheet(datas);
    this.router.navigateByUrl('timesheet');
  }

}
