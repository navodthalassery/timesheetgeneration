import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimesheetService } from '../services/timesheet.service';
import { MatDialog } from '@angular/material';
import { TaskCreateComponent } from '../task-create/task-create.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {
  employees: any;
  employee: any;
  timesheets: any;
  value: any;
  userDet: any;

  constructor(
    private empservice: EmployeeService,
    private tsTime: TimesheetService,
    private dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    this.empservice.getallemployees().subscribe(data => {
      this.employees = data;
    });

    this.tsTime.getUserId().subscribe(data => this.userDet = data);
    if (this.userDet) {
      console.log(this.userDet);
      this.value = this.userDet.id;
      this.getTimeSheet();
    }
  }

  setNewUser(event) {
    this.getTimeSheet();
  }

  getTimeSheet() {
    this.tsTime.gettimesheet(this.value).subscribe(data => {
      console.log('data ', data);
      this.timesheets = data;
    });
  }

  total(val) {
    if (this.timesheets) {
      if (val === 1) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.sunday), 0);
      } else if (val === 2) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.monday), 0);
      } else if (val === 3) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.tuesday), 0);
      } else if (val === 4) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.wednesday), 0);
      } else if (val === 5) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.thursday), 0);
      } else if (val === 6) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.friday), 0);
      } else if (val === 7) {
        return this.timesheets.reduce((summ, v) => summ += parseFloat(v.saturday), 0);
      }
    }
  }

  addTadk() {
    this.router.navigateByUrl('addtask');
  }

  addTimesheet() {
    this.router.navigateByUrl('addtimesheet');
  }
}
