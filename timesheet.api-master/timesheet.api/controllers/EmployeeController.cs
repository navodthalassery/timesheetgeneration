﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.data;

namespace timesheet.api.controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowMyOrigin")]
    public class EmployeeController : Controller
    {
        private EmployeeService employeeService = new EmployeeService();
        //public EmployeeController(EmployeeService employeeService)
        //{
        //    this.employeeService = employeeService;
        //}

        [HttpGet("[action]")]
        [EnableCors("AllowMyOrigin")]
        public IActionResult GetAll()
        {
            var items = this.employeeService.GetEmployees();
            return new ObjectResult(items);
        }
    }
}