﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowMyOrigin")]
    public class TimesheetController : Controller
    {
        private TimesheetService tsService = new TimesheetService();

        [HttpGet("[action]")]
        [EnableCors("AllowMyOrigin")]
        public IActionResult GetTimesheet(int userId)
        {
            var items = this.tsService.GetTimesheet(userId);
            return new ObjectResult(items);
        }

        [HttpGet("[action]")]
        [EnableCors("AllowMyOrigin")]
        public IActionResult GetTasks()
        {
            var items = this.tsService.GetTasks();
            return new ObjectResult(items);
        }

        [HttpPost("[action]")]
        public IActionResult SetTask([FromBody]Task task)
        {
            if (ModelState.IsValid)
            {
                var items = this.tsService.SetTask(task);
                return new ObjectResult(items);
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpPost("[action]")]
        public IActionResult SetTimesheet([FromBody]WeeklyTimesheet timesheet)
        {
            if (ModelState.IsValid)
            {
                var items = this.tsService.SetTimeSheet(timesheet);
                return new ObjectResult(items);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}