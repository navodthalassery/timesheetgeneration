﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService()
        {
            this.db = new TimesheetDb(new DbContextOptions<TimesheetDb>());
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }
    }

    public class TimesheetService
    {
        public TimesheetDb db { get; }
        public TimesheetService()
        {
            this.db = new TimesheetDb(new DbContextOptions<TimesheetDb>());
        }

        public IQueryable<WeeklyTimesheet> GetTimesheet(int userId)
        {
            var timesheet = this.db.Timesheets.Where(m => m.EmployeeId == userId);
            var tasks = this.db.Tasks;
            List<WeeklyTimesheet> query = (from ts in timesheet
                                           join tsk in tasks on ts.TaskId equals tsk.Id
                                           select new WeeklyTimesheet
                                           {
                                               Id = ts.Id,
                                               TaskId = ts.TaskId,
                                               EmployeeId = ts.EmployeeId,
                                               Sunday = ts.Sunday,
                                               Monday = ts.Monday,
                                               Tuesday = ts.Tuesday,
                                               Wednesday = ts.Wednesday,
                                               Thursday = ts.Thursday,
                                               Friday = ts.Friday,
                                               Saturday = ts.Saturday,
                                               TaskName = tsk.Name
                                           }).ToList<WeeklyTimesheet>();
            return query.AsQueryable();
        }

        public IQueryable<Task> GetTasks()
        {
            return this.db.Tasks;
        }

        public int SetTask(Task task)
        {
            this.db.Tasks.Add(task);
            return db.SaveChanges();
        }

        public int SetTimeSheet(WeeklyTimesheet timesheet)
        {
            this.db.Timesheets.Add(timesheet);
            return db.SaveChanges();
        }
    }
}
