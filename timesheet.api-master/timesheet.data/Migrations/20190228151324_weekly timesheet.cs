﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class weeklytimesheet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Timesheets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TaskId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    Sunday = table.Column<string>(maxLength: 500, nullable: true),
                    Monday = table.Column<string>(maxLength: 500, nullable: true),
                    Tuesday = table.Column<string>(maxLength: 500, nullable: true),
                    Wednesday = table.Column<string>(maxLength: 500, nullable: true),
                    Thursday = table.Column<string>(maxLength: 500, nullable: true),
                    Friday = table.Column<string>(maxLength: 500, nullable: true),
                    Saturday = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timesheets", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Timesheets");
        }
    }
}
