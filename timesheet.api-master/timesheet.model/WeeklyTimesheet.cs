﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class WeeklyTimesheet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [ForeignKey("Task")]
        [Required]
        public int TaskId { get; set; }

        [ForeignKey("Employee")]
        [Required]
        public int EmployeeId { get; set; }

        public string TaskName { get; set; }

        [StringLength(500)]
        public string Sunday { get; set; }

        [StringLength(500)]
        public string Monday { get; set; }

        [StringLength(500)]
        public string Tuesday { get; set; }


        [StringLength(500)]
        public string Wednesday { get; set; }

        [StringLength(500)]
        public string Thursday { get; set; }

        [StringLength(500)]
        public string Friday { get; set; }

        [StringLength(500)]
        public string Saturday { get; set; }


    }
}
